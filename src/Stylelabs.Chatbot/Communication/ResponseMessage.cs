﻿using Newtonsoft.Json;

namespace Stylelabs.Chatbot.Communication
{
    class ResponseMessage
    {
        [JsonProperty("response_type")]
        public string ResponseType { get; } = "in_channel";
 
        [JsonProperty("text", NullValueHandling = NullValueHandling.Ignore)]
        public string Text { get; set; }
    }
}
