using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.IO;
using Stylelabs.Chatbot.Communication;
using Stylelabs.Chatbot.Azure;
using Stylelabs.Chatbot.Models;
using System.Web;

namespace Stylelabs.Chatbot
{
    public static class Chatbot
    {
        private static string[] _allowedChannels = new string[] { "GD53C57TM", "GD2FSUBSM" };

        [FunctionName("Chatbot")]
        public static async System.Threading.Tasks.Task<IActionResult> RunAsync([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequest req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            //Parse data
            string requestBody = new StreamReader(req.Body).ReadToEnd();
            var dict = HttpUtility.ParseQueryString(requestBody);
            string json = JsonConvert.SerializeObject(dict.Cast<string>().ToDictionary(k => k, v => dict[v]));
            SlackMessage message = JsonConvert.DeserializeObject<SlackMessage>(json);

            string[] messageparameters = message.Text.Split(' ');

            try
            {
                log.Info($"ChatBot received command {messageparameters[0]} in channel {message.ChannelId} by user {message.UserName}");

                var response = new ResponseMessage();

                var azureKeyVault = new AzureKeyVault();

                switch (messageparameters[0])
                {
                    case Constants.GetPasswordCommand:
                        if(_allowedChannels.Contains(message.ChannelId))
                        {
                            response.Text = await azureKeyVault.GetSecret(messageparameters[1]);
                        }
                        else
                        {
                            response.Text = "This channel is unauthorized to use this command";
                        }
                        break;
                    default:
                        break;
                }

                return new OkObjectResult(response);
            } catch (Exception e)
            {
                log.Warning($"Request threw exception {e}");

                return new BadRequestObjectResult(e.Message);
            }
        }
    } 
}
