﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure.KeyVault;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System.IO;
using Stylelabs.Chatbot.Communication;
using Stylelabs.Chatbot.Azure;

namespace Stylelabs.Chatbot.Azure
{
    class AzureKeyVault
    {
        #region Properties

        private string _name;
        private KeyVaultClient _keyVaultClient;

        #endregion

        #region Constructor

        public AzureKeyVault()
        {
            _name = System.Environment.GetEnvironmentVariable("KEYVAULT");
            _keyVaultClient = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(new AzureServiceTokenProvider().KeyVaultTokenCallback));
        }

        #endregion

        #region Public Methods
        public async Task<string> GetSecret(string secret)
        {
            var secretBundle = await _keyVaultClient.GetSecretAsync(_name, secret);
            return secretBundle.Value;
        }

        #endregion
    }
}
